// netup - Incredibly simple network manager
// Copyright (C) 2020 Cooper Paul EdenDay
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3
// as published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#![feature(ip)]
#![forbid(
    unsafe_code,
    non_ascii_idents,
    missing_debug_implementations,
    unreachable_pub,
    rust_2018_idioms
)]
#![warn(
    clippy::pedantic,
    clippy::nursery,
    variant_size_differences,
    trivial_casts,
    trivial_numeric_casts,
    unused_import_braces,
    unused_qualifications
)]
#![allow(
    clippy::missing_docs_in_private_items,
    clippy::implicit_return,
    clippy::else_if_without_else,
    clippy::redundant_pub_crate,
    clippy::uninlined_format_args
)]

use anyhow::{Context, Result, bail, ensure};
use std::{
    env,
    fmt::Write as FmtWrite,
    fs,
    fs::File,
    io::{BufReader, SeekFrom, Write, prelude::*},
    net::{IpAddr, Ipv4Addr, Ipv6Addr, ToSocketAddrs},
    process::{Child, Command, Stdio},
    thread,
    time::Duration,
};
use tinyvec::{ArrayVec, TinyVec};

#[derive(Debug, Eq, PartialEq)]
enum ProgramMode {
    Clat,
    Plat,
}

const IP_ROUTE_2: &str = "ip";
const NFTABLES: &str = "nft";
const SYSTEMCTL: &str = "systemctl";
const CLATD: &str = "clatd.service";
const IPV4ONLY_ARPA: &str = "ipv4only.arpa.:443";

fn iproute2() -> Command {
    let mut command = Command::new(IP_ROUTE_2);
    command.stdin(Stdio::null());
    command
}

fn nftables_load(config: &str) -> Result<()> {
    let mut command = Command::new(NFTABLES);
    command.stdin(Stdio::piped());
    command.args(["-f", "-"]);
    let mut child = command.spawn()?;
    let mut stdin = child.stdin.take();
    if let Some(stdin) = &mut stdin {
        stdin.write_all(config.as_bytes())?;
        stdin.flush()?;
    }
    drop(stdin);

    if !child.wait()?.success() {
        bail!("loading nftables config failed");
    }

    Ok(())
}

fn systemctl() -> Command {
    let mut command = Command::new(SYSTEMCTL);
    command.stdin(Stdio::null());
    command
}

const fn to4(ip: IpAddr) -> Option<Ipv4Addr> {
    match ip {
        IpAddr::V4(ip) => Some(ip),
        IpAddr::V6(_) => None,
    }
}

const fn to_ug6(ip: IpAddr) -> Option<Ipv6Addr> {
    match ip {
        IpAddr::V4(_) => None,
        IpAddr::V6(ip) => {
            if !ip.is_multicast() && ip.is_unicast_global() {
                Some(ip)
            } else {
                None
            }
        }
    }
}

fn collect2<Iter: Iterator>(iter: Iter) -> ArrayVec<[Option<Iter::Item>; 2]>
where
    Iter::Item: Ord,
{
    let mut collection = ArrayVec::new();
    drop(collection.fill(iter.map(Some)));
    collection.sort_unstable();
    collection
}

fn until_dns(mut interfaces: Option<&mut TinyVec<[String; 16]>>) {
    loop {
        if let Some(interfaces) = &mut interfaces {
            set_up(interfaces);
        }

        thread::sleep(Duration::from_millis(100));

        let addresses = IPV4ONLY_ARPA
            .to_socket_addrs()
            .map(|iter| iter.map(|socket| socket.ip()).filter_map(to4))
            .map(collect2);

        if addresses
            .is_ok_and(|a| a.as_slice() == ["192.0.0.170".parse().ok(), "192.0.0.171".parse().ok()])
        {
            break;
        }
    }
}

fn get_program_mode() -> Result<ProgramMode> {
    let mut program_mode = ProgramMode::Clat;

    let args = env::args();
    if args.len() == 0 {
        bail!("fatal: args length must not be zero");
    }

    for (arg_num, arg) in args.enumerate() {
        if arg_num == 0 {
            ensure!(&arg == "netup");
        } else if arg_num == 1 {
            if &arg == "plat" {
                program_mode = ProgramMode::Plat;
            }
        } else if arg_num > 1 {
            bail!("Only one extra argument allowed.");
        }
    }

    Ok(program_mode)
}

fn get_routes(ip: &str) -> Vec<u8> {
    let routes_cmd = iproute2().args(["-6", "route", "get", ip]).output();
    routes_cmd.map(|o| o.stdout).unwrap_or_default()
}

fn target_ips() -> TinyVec<[Option<Ipv6Addr>; 8]> {
    let mut target_ips: TinyVec<[_; 8]> = IPV4ONLY_ARPA
        .to_socket_addrs()
        .map(|iter| iter.map(|socket| socket.ip()).filter_map(to_ug6).map(Some).collect())
        .unwrap_or_default();
    target_ips.retain(Option::is_some);
    target_ips.sort_unstable();

    target_ips
}

fn program_mode_switch<T: AsRef<str>>(program_mode: &ProgramMode, interfaces: &[T]) -> Result<()> {
    match program_mode {
        ProgramMode::Clat => {
            drop(systemctl().args(["start", CLATD]).status());
        }
        ProgramMode::Plat => {
            plat_load_firewall(interfaces).context("failed to load firewall config")?;

            if let Ok(v4_ip) = fs::read_to_string("/etc/netup_v4_ip.conf") {
                let mut cmd = iproute2();
                let mut cmd = cmd.args(["-4", "address", "add"]);

                for word in v4_ip.split_whitespace() {
                    cmd = cmd.arg(word);
                }

                cmd.spawn().context("failed to add address from /etc/netup_v4_ip.conf")?.wait()?;

                if let Ok(v4_route) = fs::read_to_string("/etc/netup_v4_route.conf") {
                    let mut cmd = iproute2();
                    let mut cmd = cmd.args(["-4", "route", "add"]);

                    for word in v4_route.split_whitespace() {
                        cmd = cmd.arg(word);
                    }

                    cmd.spawn()
                        .context("failed to add route from /etc/netup_v4_route.conf")?
                        .wait()?;
                }
            }
        }
    }

    Ok(())
}

fn main() -> Result<()> {
    let mut interfaces = TinyVec::new();
    let program_mode = get_program_mode()?;
    if program_mode == ProgramMode::Plat {
        plat_load_firewall(&interfaces).context("failed to load initial firewall config")?;
    }

    let mon = File::open("/dev/kmsg").context("failed to open /dev/kmsg")?;
    let mut mon_reader = BufReader::new(mon); // The buffer must be at least 2048 bytes.

    until_dns(Some(&mut interfaces));

    program_mode_switch(&program_mode, &interfaces)?;

    let interfaces_copy = interfaces.clone();
    let mut interface_carrier_ups: Vec<_> = interfaces.into_iter().map(File::open).collect();
    let mut ups_buffer = String::with_capacity(4);
    let mut old_carrier_ups: TinyVec<[Option<u32>; 16]> =
        interface_carrier_ups.iter().map(|_| None).collect();
    let mut old_routes_vec = Vec::new();
    let mut first_loop = true;
    let mut mon_message = String::new();
    loop {
        let mut do_restart = false;
        thread::sleep(Duration::from_millis(100));

        mon_message.clear();
        {
            let result = mon_reader.read_line(&mut mon_message);
            if let Err(e) = result {
                eprintln!("Failed to read from /dev/kmsg: {:?}", e);

                continue;
            }
        }

        for (index, file) in interface_carrier_ups.iter_mut().flatten().enumerate() {
            if file.seek(SeekFrom::Start(Default::default())).is_err() {
                continue;
            }

            ups_buffer.clear();
            if file.read_to_string(&mut ups_buffer).is_err() {
                continue;
            }

            let ups: Option<u32> =
                ups_buffer.lines().next().unwrap_or_default().parse().map(Some).unwrap_or_default();

            let old_ups = old_carrier_ups.get(index).copied().unwrap_or_default();
            if ups != old_ups {
                if let Some(target) = old_carrier_ups.get_mut(index) {
                    *target = ups;
                }

                if old_ups.is_some() {
                    do_restart = true;
                }

                break;
            }
        }

        let ip = target_ips()
            .iter()
            .next()
            .map(|ip| format!("{}", ip.unwrap_or(Ipv6Addr::UNSPECIFIED)))
            .unwrap_or_default();
        let routes_vec = get_routes(&ip);

        if routes_vec != old_routes_vec {
            if first_loop {
                first_loop = false;
            } else {
                do_restart = true;
            }

            old_routes_vec.clone_from(&routes_vec);
        }

        if do_restart {
            thread::sleep(Duration::from_millis(100));
            until_dns(None);

            match program_mode {
                ProgramMode::Clat => {
                    if memchr::memmem::find(&routes_vec, ip.as_bytes()).is_none() {
                        let mut stop_loop = false;
                        loop {
                            let temp_routes = get_routes(&ip);

                            if memchr::memmem::find(&temp_routes, ip.as_bytes()).is_some() {
                                old_routes_vec.clone_from(&temp_routes);
                                stop_loop = true;
                            }

                            thread::sleep(Duration::from_millis(100));

                            if stop_loop {
                                break;
                            }
                        }

                        until_dns(None);
                    }

                    drop(systemctl().args(["restart", CLATD]).status());
                }
                ProgramMode::Plat => {
                    plat_load_firewall(interfaces_copy.as_ref())
                        .context("failed to load firewall config")?;
                }
            }

            thread::sleep(Duration::from_millis(400));
        }
    }
}

fn plat_load_firewall<T: AsRef<str>>(interfaces: &[T]) -> Result<()> {
    let mut allow_statements = String::with_capacity(512);
    for interface in interfaces {
        let interface = interface.as_ref();

        let interface = interface
            .strip_prefix("/sys/class/net/")
            .unwrap_or_default()
            .strip_suffix("/carrier_up_count")
            .unwrap_or_default();

        let output = iproute2()
            .args([
                "-6",
                "address",
                "show",
                "dev",
                interface,
                "primary",
                "-dadfailed",
                "-deprecated",
                "scope",
                "global",
            ])
            .output()
            .context("getting global addresses from interface failed")?;
        if !output.status.success() {
            eprintln!("Interface {}...", interface);
            bail!("ip command did not exit with success code");
        }
        let stdout = output.stdout;
        let stdout_str = std::str::from_utf8(&stdout).context("ip address output not UTF-8")?;
        for candidate_str in stdout_str.split_whitespace() {
            let candidate_str = candidate_str.trim();
            if let Some((candidate_ip_str, _)) = candidate_str.rsplit_once("/64") {
                let address: Ipv6Addr = candidate_ip_str.parse()?;
                let seg_s = address.segments();
                let new_seg = [seg_s[0], seg_s[1], seg_s[2], seg_s[3], 0, 0, 0, 0];

                let new_address = Ipv6Addr::from(new_seg);
                writeln!(
                    &mut allow_statements,
                    "ip6 daddr 64:ff9b::/96 ip6 saddr {}/64 accept",
                    new_address
                )?;
            }
        }
    }

    let input_config =
        fs::read_to_string("/etc/nftables.conf").context("failed to read /etc/nftables.conf")?;
    let output_config = input_config.replace("$PLAT_ACCEPT\n", &allow_statements);

    nftables_load(&output_config)?;

    Ok(())
}

fn set_up(interfaces: &mut TinyVec<[String; 16]>) {
    interfaces.clear();
    let mut children = TinyVec::<[Option<Child>; 16]>::default();

    if let Ok(config) = fs::read_to_string("/etc/netup.conf") {
        for line in config.lines() {
            if let Some(word) = line.split_whitespace().next() {
                if !word.starts_with('#') {
                    iproute2().args(["link", "set", word, "up"]).spawn().map_or_else(
                        |_| {
                            eprintln!("Could not start command '{}'.", IP_ROUTE_2);
                        },
                        |child| {
                            interfaces.push(format!("/sys/class/net/{}/carrier_up_count", word));
                            children.push(Some(child));
                        },
                    );
                }
            }
        }
    }

    for child in children.iter_mut().flatten() {
        drop(child.wait());
    }
}
